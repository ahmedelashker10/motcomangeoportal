//
//  ViewController.m
//  MOTC Oman GeoPortal
//
//  Created by Ahmed Elashker on 5/4/15.
//  Copyright (c) 2015 ESRI Northeast Africa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize viewLoading, viewDim, lblLoading, indicator, btnRetry, btnBack;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initData];
    
    [self adjustView:[UIApplication sharedApplication].statusBarOrientation];
    
    [self loadWebView];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustView:toInterfaceOrientation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    // webview
    _webView = [[UIWebView alloc] init];
    _webView.delegate = self;
    
    viewDim = [[UIView alloc] init];
    
    viewLoading = [[UIView alloc] init];
    viewLoading.alpha = 0.7;
    viewLoading.backgroundColor = [UIColor grayColor];
    viewLoading.layer.cornerRadius = 5;
    
    lblLoading = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 50)];
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.backgroundColor = [UIColor clearColor];
    
    indicator = [[UIActivityIndicatorView alloc] init];
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    
    btnRetry = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x, self.view.center.y + 40, 50, 50)];
    [btnRetry setTitle:@"Retry" forState:UIControlStateNormal];
    [btnRetry setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnRetry addTarget:self action:@selector(retry) forControlEvents:UIControlEventTouchUpInside];
    [btnRetry.layer setBorderWidth:1];
    [btnRetry.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btnRetry.layer setCornerRadius:5];
    
    btnBack = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, 60, 50, 50)];
    [btnBack setTitle:@"Back" forState:UIControlStateNormal];
    [btnBack setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(webViewBack) forControlEvents:UIControlEventTouchUpInside];
    [btnBack.layer setBorderWidth:1];
    [btnBack.layer setBorderColor:[UIColor blackColor].CGColor];
    [btnBack.layer setCornerRadius:5];
}

- (void) adjustView:(UIInterfaceOrientation)orientation {
    
    if(UIInterfaceOrientationIsLandscape(orientation)) {
        _webView.frame = CGRectMake(0, 20, 1024, 748);
    }
    else {
        _webView.frame = CGRectMake(0, 20, 768, 1004);
    }
    
    CGRect frm = _webView.frame;
    frm.origin.y = 0;
    frm.size.height += 20;
    
    viewDim.frame = frm;
    
    [self adjustCenters];
}

- (void)adjustCenters
{
    viewLoading.center = viewDim.center;
    indicator.center = CGPointMake(lblLoading.center.x, lblLoading.center.y + 40);
    btnRetry.center = indicator.center;
    
    btnBack.center = CGPointMake(viewDim.frame.size.width - 60, 40);
}

- (void)labelAdjustsView
{
    [lblLoading sizeToFit];
    
    CGSize sze;
    sze.height = 100;
    sze.width = lblLoading.frame.size.width + 20;
    
    viewLoading.frame = CGRectMake(0, 0, sze.width, sze.height);
    viewLoading.center = viewDim.center;
}

- (void) loadWebView {
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSString *strUrl = @"http://gis.motc.gov.om/index.html";
    //NSString *strUrl = @"http://46.40.225.90/motcpublic";
    //NSString *strUrl = @"https://twowritingteachers.files.wordpress.com/2012/08/smith_crafttable_one-green-apple.pdf";
    //NSString *strUrl = @"https://google.com";
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [_webView loadRequest:request];
    
    [self.view addSubview:_webView];
}

- (void) retry {
    
    [self loadWebView];
}

- (void) webViewBack
{
    [_webView goBack];
    
    [btnBack removeFromSuperview];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL isPdf = [[[request.URL pathExtension] lowercaseString]
                  isEqualToString:@"pdf"];
    
    if (isPdf)
    {
        [_webView addSubview:btnBack];
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [btnRetry removeFromSuperview];
    
    [self.view addSubview:viewDim];
    
    [viewDim addSubview:viewLoading];
    
    lblLoading.text = @"Loading";
    
    [self labelAdjustsView];
    [self adjustCenters];
    
    [viewLoading addSubview:lblLoading];
    
    [indicator startAnimating];
    [viewLoading addSubview:indicator];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (![self isNetworkAvailable])
    {
        [self failureHandler:@"Check your internet connection"];
        return;
    }
    
    [lblLoading removeFromSuperview];
    
    [indicator stopAnimating];
    
    [indicator removeFromSuperview];
    
    [viewLoading removeFromSuperview];
    
    [btnRetry removeFromSuperview];
    
    [viewDim removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self failureHandler:error.localizedDescription];
}

- (void)failureHandler:(NSString*)errorStr
{
    [indicator stopAnimating];
    [indicator removeFromSuperview];
    
    lblLoading.text = errorStr;
    
    [self labelAdjustsView];
    [self adjustCenters];
    
    [viewLoading addSubview:btnRetry];
}

- (BOOL) isNetworkAvailable
{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReachOnExistingConnection =     success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    if( canReachOnExistingConnection )
        NSLog(@"Network available");
    else
        NSLog(@"Network not available");
    
    return canReachOnExistingConnection;
}

@end
