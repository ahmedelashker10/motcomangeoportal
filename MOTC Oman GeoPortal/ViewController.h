//
//  ViewController.h
//  MOTC Oman GeoPortal
//
//  Created by Ahmed Elashker on 5/4/15.
//  Copyright (c) 2015 ESRI Northeast Africa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@interface ViewController : UIViewController <UIWebViewDelegate, NSURLConnectionDataDelegate>

@property UIWebView *webView;
@property UIView *viewDim;
@property UIView *viewLoading;
@property UILabel *lblLoading;
@property UIActivityIndicatorView *indicator;
@property UIButton *btnRetry;
@property UIButton *btnBack;
@end

