//
//  main.m
//  MOTC Oman GeoPortal
//
//  Created by Ahmed Elashker on 5/4/15.
//  Copyright (c) 2015 ESRI Northeast Africa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
