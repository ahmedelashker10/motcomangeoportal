//
//  AppDelegate.h
//  MOTC Oman GeoPortal
//
//  Created by Ahmed Elashker on 5/4/15.
//  Copyright (c) 2015 ESRI Northeast Africa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

